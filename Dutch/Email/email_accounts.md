# Email Accounts

1. Een email pakket kiezen
2. Mailbox aanmaken
3. Inloggen webmail

## Een email pakket kiezen

Voordat je een email account kunt aanmaken moet je je domein toewijzen aan een email pakket. Wij voorzien drie pakketten .

Gratis - 5 email adressen
Standaard - 15 email adressen
Profesioneel - 50 email adressen
Via het klantenpaneel selecteer je in het menu "Email" en klik je op het "+" symbool.

In de pop-up selecteert je jouw domeinnaam en kies je een geschikt pakket en klik je op "Save Changes"

5 email accounts zijn gratis en deze zullen altijd gratis blijven.
email02

## Een mailbox aanmaken

Wanneer je een domein hebt toegevoegd aan een email pakket, dan kan je nu je eerste email account aanmaken. Klik op configure.

email03

Nu kan je je eerste email account aanmaken. Klik op het "+" symbool en voer de nodige informatie in en klik op "Save Changes". Je email account is nu klaar voor gebruik.

email04

## Inloggen webmail

Inloggen in de webmail client kan je via de url mail.phasehosting.io. Als gebruikersnaam geef je je gekozen email in en vul dan je wachtwoord in en klik op aanmelden.

email05

Nadat je succesvol bent aangemeld kan je je email account configureren naar eigen wens en ben je klaar om e-mails te versturen.